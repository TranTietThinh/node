export interface Brand {
    _id: string
    name: string
    logoImg: string
    description: string
    story: string
}

export interface BrandSchemaModel extends Omit<Brand, '_id'> {}

export interface FO_Brand extends Brand {}

export interface FO_BrandTile extends Omit<Brand, 'description' | 'story'> {}
