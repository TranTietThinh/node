export interface User {
	id: number;
	username: string;
	password: string;
	access_token: string;
}

export interface UserSchemaModel extends Omit<User, 'id' | 'access_token'> {}

export interface FO_User extends Omit<User, 'password'> {}
