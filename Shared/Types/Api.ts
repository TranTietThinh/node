export interface ResponseSucceed<T> {
	result: T;
}
export interface ResponseError {
	errorCode: string;
	errorMessage: string;
	errorPayload: any;
}

export interface ResponseApi<T> extends ResponseSucceed<T>, ResponseError {}
