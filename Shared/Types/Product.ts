export interface Product {
    _id: string
    price: number
    name: string
    scale: string
    sku: string
    description: string
    story: string
    promotion: number
    imgs: string[]
    brandId: string
    totalSale: number
}

export interface ProductSchemaModel extends Omit<Product, '_id'> {}

export interface FO_Product extends Product {}

export interface FO_ProductTile extends Omit<Product, 'sku' | 'description' | 'story' | 'totalSale'> {}
