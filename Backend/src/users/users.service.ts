import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { User } from './users.schema';
import * as CryptoJS from 'crypto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}
  async findOne(username: string): Promise<User | undefined> {
    return this.userModel.findOne({
      username,
    });
  }

  async createUser(username: string, password: string): Promise<User> {
    const HashService = CryptoJS.createHash('sha512');
    return this.userModel.create({
      username: username,
      password: HashService.update(password).digest('hex'),
    });
  }
}
