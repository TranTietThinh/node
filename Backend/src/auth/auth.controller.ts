import { Body, Controller, Post } from '@nestjs/common';
import { FO_User, ResponseError } from 'shared';
import { UsersService } from 'src/users/users.service';

import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly userService: UsersService,
    private readonly authService: AuthService,
  ) {}

  @Post('sign-up')
  async create(
    @Body('username') _username: string,
    @Body('password') _password: string,
  ) {
    const { password, ...result } = await this.userService.createUser(
      _username,
      _password,
    );
    return result;
  }

  @Post('sign-in')
  async signIn(
    @Body('username') _username: string,
    @Body('password') _password: string,
  ): Promise<FO_User | ResponseError> {
    const user = await this.authService.validateUser(_username, _password);
    if (user) {
      const { password, ...result } = user;
      return result;
    }
    return {
      errorCode: '00001',
      errorMessage: 'Wrong username/password',
      errorPayload: null,
    };
  }
}
