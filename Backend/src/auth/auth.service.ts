import { Injectable } from '@nestjs/common';
import * as CryptoJS from 'crypto';
import { User } from 'shared/Types';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<User | null> {
    const user = await this.usersService.findOne(username);
    const passwordHash = CryptoJS.createHash('sha512').update(pass, 'utf8');
    if (user && user.password === passwordHash.digest('hex')) {
      return {
        username: user.username,
        id: user.id,
        password: user.password,
        access_token: await this.generateToken({
          id: user.id,
          password: user.password,
          username: user.username,
        }),
      };
    }
    return null;
  }

  async generateToken(user: Omit<User, 'access_token'>): Promise<string> {
    const payload = { username: user.username, sub: user.id };
    return this.jwtService.sign(payload);
  }
}
