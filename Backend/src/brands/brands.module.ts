import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'

import { BrandsResolver } from './brands.resolver'
import { Brand, BrandSchema } from './brands.schema'
import { BrandsService } from './brands.service'

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Brand.name, schema: BrandSchema }]),
  ],
  providers: [BrandsService, BrandsResolver],
})
export class BrandsModule {}
