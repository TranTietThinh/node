import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { BrandSchemaModel } from 'shared';

@Schema()
export class Brand extends Document implements BrandSchemaModel {
  @Prop({ required: true })
  logoImg: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  description: string;

  @Prop({ required: true })
  story: string;
}

export const BrandSchema = SchemaFactory.createForClass(Brand);
