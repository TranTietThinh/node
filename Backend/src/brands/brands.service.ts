import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { Brand } from './brands.schema';
import { BrandInput } from './brands.model';

@Injectable()
export class BrandsService {
  constructor(
    @InjectModel(Brand.name) private brandModel: Model<Brand>,
  ) {}
  async findOne(id: string): Promise<Brand | undefined> {
    return this.brandModel.findOne({
      _id: id,
    });
  }

  async createBrand(brand: BrandInput): Promise<BrandInput> {
    const createdBrand = new this.brandModel(brand);
    return await createdBrand.save();
  }
}
