import { Field, Int, ObjectType, InputType } from '@nestjs/graphql';
import { Brand, BrandSchemaModel } from 'shared';

@ObjectType()
export class BrandModel implements BrandSchemaModel {
  @Field()
  _id: string;
  @Field()
  name: string;
  @Field()
  description: string;
  @Field()
  story: string;
  @Field()
  logoImg: string;
}

@InputType()
export class BrandInput implements BrandSchemaModel {
  @Field()
  name: string;
  @Field()
  description: string;
  @Field()
  story: string;
  @Field()
  logoImg: string;
}
