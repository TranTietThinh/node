import { Args, Mutation, Query, Resolver } from '@nestjs/graphql'

import { BrandInput, BrandModel } from './brands.model'
import { BrandsService } from './brands.service'

@Resolver(of => BrandModel)
export class BrandsResolver {
  constructor(private brandService: BrandsService) {}

  @Query(returns => BrandModel)
  async getBrandById(@Args('id') id: string) {
    return this.brandService.findOne(id);
  }

  @Mutation(of => BrandModel)
  async createBrand(@Args('input') input: BrandInput) {
    return this.brandService.createBrand(input);
  }
}
