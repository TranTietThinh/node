import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { ProductInput, ProductDetail, ProductTile } from './products.model';
import { ProductsService } from './products.service';
import { FO_Product } from 'shared';

@Resolver(of => ProductDetail)
export class ProductsResolver {
  constructor(private productService: ProductsService) {}

  @Query(returns => ProductDetail)
  async getProductById(@Args('id') id: string): Promise<FO_Product> {
    return this.productService.findOne(id).then(e => e as FO_Product);
  }

  @Query(returns => [ProductTile])
  async getProductsByBrand(@Args('id') id: string): Promise<FO_Product[]> {
    return this.productService
      .findAll({ brandId: id })
      .then(result => result as FO_Product[]);
  }

  @Query(returns => [ProductTile])
  async getNewArrivalProducts(): Promise<FO_Product[]> {
    return this.productService
      .getNewArrivalProduct()
      .then(result => result as FO_Product[]);
  }

  @Query(returns => [ProductTile])
  async getTopSellingProducts(): Promise<FO_Product[]> {
    return this.productService
      .getTopSellingProducts()
      .then(result => result as FO_Product[]);
  }

  @Mutation(of => ProductDetail)
  async createProduct(@Args('input') input: ProductInput) {
    return this.productService.createProduct(input);
  }
}
