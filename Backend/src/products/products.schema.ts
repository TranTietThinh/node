import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ProductSchemaModel } from 'shared';

@Schema()
export class Product extends Document implements ProductSchemaModel {
  @Prop({ required: true })
  imgs: string[];

  @Prop({ required: true })
  price: number;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true })
  scale: string;

  @Prop({ required: true })
  sku: string;

  @Prop({ required: true })
  description: string;

  @Prop({ required: true })
  story: string;

  @Prop({ required: true })
  promotion: number;

  @Prop({ required: true })
  brandId: string;

  @Prop({ required: true })
  totalSale: number;
}

export const ProductSchema = SchemaFactory.createForClass(Product);
