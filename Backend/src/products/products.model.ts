import { Field, InputType, Int, ObjectType } from '@nestjs/graphql';
import { FO_ProductTile, ProductSchemaModel } from 'shared';

@ObjectType()
export class ProductDetail implements ProductSchemaModel {
  @Field()
  _id: string;
  @Field(type => Int)
  price: number;
  @Field()
  name: string;
  @Field()
  scale: string;
  @Field()
  sku: string;
  @Field()
  description: string;
  @Field()
  story: string;
  @Field(type => Int)
  promotion: number;
  @Field(type => [String])
  imgs: string[];
  @Field()
  brandId: string;
  @Field(type => Int)
  totalSale: number;
}
@ObjectType()
export class ProductTile implements FO_ProductTile {
  @Field()
  _id: string;
  @Field(type => Int)
  price: number;
  @Field()
  name: string;
  @Field()
  scale: string;
  @Field(type => Int)
  promotion: number;
  @Field(type => [String])
  imgs: string[];
  @Field()
  brandId: string;
}

@InputType()
export class ProductInput implements ProductSchemaModel {
  @Field(type => Int)
  price: number;
  @Field()
  name: string;
  @Field()
  scale: string;
  @Field()
  sku: string;
  @Field()
  description: string;
  @Field()
  story: string;
  @Field(type => Int)
  promotion: number;
  @Field(type => [String])
  imgs: string[];
  @Field()
  brandId: string;
  @Field(type => Int)
  totalSale: number;
}
