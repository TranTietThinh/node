import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, FilterQuery } from 'mongoose';

import { Product } from './products.schema';
import { ProductInput } from './products.model';

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel(Product.name) private productModel: Model<Product>,
  ) {}
  async findOne(id: string): Promise<Product | undefined> {
    return this.productModel.findOne({
      _id: id,
    });
  }

  async findAll(filter: FilterQuery<Product>): Promise<Product[]> {
    return this.productModel.find(filter).exec();
  }

  async createProduct(product: ProductInput): Promise<ProductInput> {
    const createdProduct = new this.productModel(product);
    return await createdProduct.save();
  }

  async getNewArrivalProduct(): Promise<Product[]> {
    return this.productModel.find().limit(10).exec();
  }

  async getTopSellingProducts(): Promise<Product[]> {
    return this.productModel.find().limit(4).exec();
  }
}
