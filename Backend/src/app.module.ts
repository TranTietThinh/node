import { Module, CacheKey } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { GraphQLModule } from '@nestjs/graphql'

import { AppController } from './app.controller'
import { AppService } from './app.service'
import { AuthModule } from './auth/auth.module'
import { DatabaseModule } from './database/database.module'
import { UsersModule } from './users/users.module'
import { ProductsModule } from './products/products.module'
import { BrandsModule } from './brands/brands.module'
import { MemcachedCache } from 'apollo-server-memcached'

const cache: any = new MemcachedCache(
  ['memcached-server-1', 'memcached-server-2', 'memcached-server-3'],
  { retries: 10, retry: 10000 }, // Options
)
cache.delete = cache.flush

@Module({
  imports: [
    AuthModule,
    UsersModule,
    ProductsModule,
    BrandsModule,
    DatabaseModule,
    ConfigModule.forRoot({ isGlobal: true }),
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
      debug: false
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
