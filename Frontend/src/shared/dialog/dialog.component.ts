import { Component, Inject } from '@angular/core'
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog'
import { DialogParams } from './dialog.constant'

@Component({
  selector: 'dialog-modal',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent {
  constructor(
    public dialogRef: MatDialogRef<unknown>,
    @Inject(MAT_DIALOG_DATA) public data: DialogParams
  ) {}

  onSubmit(result: boolean): void {
    this.dialogRef.close(result)
  }
}
