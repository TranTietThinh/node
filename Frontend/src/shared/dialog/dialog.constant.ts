import { MatDialogConfig } from '@angular/material/dialog'

export interface ConfirmDialogParams {
  title?: string
  message: string
  yesText?: string
  noText: string
  matDialogOptions?: MatDialogConfig
}

export interface AlertDialogParams {
  title?: string
  message: string
  okText?: string
  matDialogOptions?: MatDialogConfig
}

export interface DialogParams
  extends Omit<ConfirmDialogParams, 'matDialogOptions'>,
    Omit<AlertDialogParams, 'matDialogOptions'> {
  type: 'alert' | 'confirm'
}
