import { Injectable } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

import { DialogComponent } from './dialog.component'
import { AlertDialogParams, ConfirmDialogParams } from './dialog.constant'

@Injectable()
export class DialogService {
  constructor(private matDialog: MatDialog) {}

  confirm(params: ConfirmDialogParams): Observable<boolean> {
    const { matDialogOptions, ...restParams } = params
    return this.matDialog
      .open(DialogComponent, {
        ...matDialogOptions,
        data: {
          ...restParams,
          type: 'confirm',
        },
      })
      .afterClosed()
      .pipe(map((result) => !!result))
  }

  alert(params: AlertDialogParams): Observable<boolean> {
    const { matDialogOptions, ...restParams } = params

    return this.matDialog
      .open(DialogComponent, {
        ...matDialogOptions,
        data: {
          ...restParams,
          type: 'alert',
        },
      })
      .afterClosed()
  }
}
