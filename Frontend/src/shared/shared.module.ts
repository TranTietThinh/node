import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { MatDialogModule } from '@angular/material/dialog'
import { TranslateModule } from '@ngx-translate/core'

import { DialogComponent } from './dialog/dialog.component'
import { DialogService } from './dialog/dialog.service'

@NgModule({
  declarations: [DialogComponent],
  imports: [MatDialogModule, CommonModule],
  exports: [CommonModule, TranslateModule],
  providers: [DialogService],
})
export class SharedModule {}
