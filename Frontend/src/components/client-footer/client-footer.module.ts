import { NgModule } from '@angular/core'
import { ClientFooterComponent } from './client-footer.component'

@NgModule({
  declarations: [ClientFooterComponent],
  exports: [ClientFooterComponent],
})
export class ClientFooterModule {}
