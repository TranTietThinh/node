import { Component } from '@angular/core'

@Component({
  selector: 'client-footer',
  templateUrl: './client-footer.component.html',
  styleUrls: ['./client-footer.component.scss'],
})
export class ClientFooterComponent {}
