import { NgModule } from '@angular/core'
import { ClientHeaderComponent } from './client-header.component'
import { CommonModule } from '@angular/common'

@NgModule({
  declarations: [ClientHeaderComponent],
  imports: [CommonModule],
  exports: [ClientHeaderComponent],
})
export class ClientHeaderModule {}
