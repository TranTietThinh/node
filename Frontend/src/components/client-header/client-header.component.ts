import { Component } from '@angular/core'

@Component({
  selector: 'client-header',
  templateUrl: './client-header.component.html',
  styleUrls: ['./client-header.component.scss'],
})
export class ClientHeaderComponent {
  activeMenu = {
    home: false,
    products: false,
    brands: false, 
    contact: false
  }
}
