import { NgModule } from '@angular/core'
import { MatIconModule } from '@angular/material/icon'

import { ProductComponent } from './product-tile.component'
import { CommonModule } from '@angular/common'

@NgModule({
  declarations: [ProductComponent],
  imports: [MatIconModule, CommonModule],
  exports: [ProductComponent]
})
export class ProductComponentModule {}
