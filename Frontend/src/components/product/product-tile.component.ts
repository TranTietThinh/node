import { Component, Input } from '@angular/core'
import { FO_Product } from 'shared'
@Component({
  selector: 'product-tile-component',
  templateUrl: './product-tile.component.html',
  styleUrls: ['./product-tile.component.scss'],
})
export class ProductComponent {
  @Input() readonly data: FO_Product | null = null
}
