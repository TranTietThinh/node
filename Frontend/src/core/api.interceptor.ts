import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http'
import { Injectable, Injector } from '@angular/core'
import { Observable } from 'rxjs'

import { AuthService } from './auth.service'

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private injector: Injector) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let result = request
    const authService = this.injector.get(AuthService)
    if (authService.isAuthenticated()) {
      result = request.clone({
        setHeaders: {
          Authorization: `Bearer ${authService.getToken()}`,
        },
      })
    }
    return next.handle(result)
  }
}
