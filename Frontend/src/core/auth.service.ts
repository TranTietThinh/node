import { Injectable } from '@angular/core'
import * as decode from 'jwt-decode'
@Injectable()
export class AuthService {
  public getToken(): string | null {
    // return localStorage.getItem('token') 
    return ''
  }
  public isAuthenticated(): boolean {
    return !!this.getToken()
  }
}
