import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { ClientFooterModule } from '@component/client-footer/client-footer.module'
import { ClientHeaderModule } from '@component/client-header/client-header.module'
import { CommonDirectiveModule } from '@directive/common-directive.module'
import { IvyCarouselModule } from 'angular-responsive-carousel'
import { ProductComponentModule } from 'src/components/product/product-tile.module'

import { HomePageComponent } from './home.component'
import { HomeService } from './home.service'

@NgModule({
  declarations: [HomePageComponent],
  imports: [
    CommonModule,
    IvyCarouselModule,
    ClientHeaderModule,
    ClientFooterModule,
    CommonDirectiveModule,
    ProductComponentModule,
  ],
  providers: [HomeService]
})
export class HomeModule {}
