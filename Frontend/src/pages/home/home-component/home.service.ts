import { Injectable } from '@angular/core'
import { Apollo } from 'apollo-angular'
import {
  GetNewArrivalProductsDocument,
  GetNewArrivalProductsQuery,
  GetTopSellingProductsQuery,
  GetTopSellingProductsDocument,
} from '@graphql'
import { Observable, from } from 'rxjs'
import { map } from 'rxjs/operators'
import { FO_ProductTile } from 'shared'

@Injectable()
export class HomeService {
  constructor(private apollo: Apollo) {
    this.apollo.use('client')
  }

  getNewArrival(): Observable<FO_ProductTile[]> {
    return this.apollo
      .query<GetNewArrivalProductsQuery>({
        query: GetNewArrivalProductsDocument,
      })
      .pipe(map((result) => result.data?.getNewArrivalProducts || []))
  }

  getTopSellingProducts(): Observable<FO_ProductTile[]> {
    return this.apollo
      .query<GetTopSellingProductsQuery>({
        query: GetTopSellingProductsDocument,
      })
      .pipe(map((result) => result.data?.getTopSellingProducts || []))
  }
}
