import { Component, OnInit } from '@angular/core'
import { Observable, of } from 'rxjs'
import { FO_ProductTile } from 'shared'

import { HomeService } from './home.service'

@Component({
  styleUrls: ['./home.component.scss'],
  templateUrl: './home.component.html',
})
export class HomePageComponent implements OnInit {
  newArrival$: Observable<FO_ProductTile[]> = of([])
  topSelling$: Observable<FO_ProductTile[]> = of([])

  constructor(private service: HomeService) {}
  
  ngOnInit(): void {
    this.newArrival$ = this.service.getNewArrival()
    this.topSelling$ = this.service.getTopSellingProducts()
  }
}
