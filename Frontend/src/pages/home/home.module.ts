import { NgModule } from '@angular/core'

import { HomeModule } from './home-component/home.module'
import { HomeRoutingModule } from './home.routing'

@NgModule({
  imports: [HomeRoutingModule, HomeModule],
})
export class HomePageModule {}
