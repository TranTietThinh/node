import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { environment } from 'src/environments/environment'
import { FO_User, ResponseApi } from 'shared'
import { Observable } from 'rxjs'
import { first } from 'rxjs/operators'
@Injectable()
export class LoginService {
  constructor(private httpService: HttpClient) {}

  login(username: string, password: string): Observable<ResponseApi<FO_User>> {
    return this.httpService
      .post<ResponseApi<FO_User>>(`${environment.api}/auth/sign-in`, {
        username,
        password,
      })
      .pipe(first())
  }
}
