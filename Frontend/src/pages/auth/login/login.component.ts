import { Component, OnInit } from '@angular/core'
import { FormControl, FormGroup } from '@angular/forms'
import { Router } from '@angular/router'
import { delay, filter, tap } from 'rxjs/operators'

import { LoginService } from './login.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  viewPassword = false
  loading = false
  errorMessage = ''
  form: FormGroup = new FormGroup({
    username: new FormControl(),
    password: new FormControl(),
  })
  constructor(private service: LoginService, private router: Router) {}

  ngOnInit(): void {}

  login(): void {
    const { username, password } = this.form.value
    this.service
      .login(username, password)
      .pipe(
        tap(() => {
          this.loading = true
        }),
        delay(1000),
        tap(() => {
          this.loading = false
        }),
        filter((response) => {
          if (response.errorCode) {
            this.errorMessage = response.errorMessage
          }
          return !response.errorCode
        }),
        tap(() =>
          setTimeout(() => {
            this.router.navigate(['/'])
          }, 700)
        )
      )
      .subscribe()
  }
}
