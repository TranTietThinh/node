import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { AuthRoutingModule } from './auth-routing.module'
import { LoginModule } from './login/login.module'
import { ForgotModule } from './forgot/forgot.module'
import { SignUpModule } from './sign-up/sign-up.module'
import { SignUpComponent } from './sign-up/sign-up.component'

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AuthRoutingModule,
    LoginModule,
    ForgotModule,
    SignUpModule,
  ],
})
export class AuthModule {}
