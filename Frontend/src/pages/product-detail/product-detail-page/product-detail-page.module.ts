import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { ProductDetailPageComponent } from './product-detail-page.component'
import { RouterModule } from '@angular/router'
import { ClientHeaderModule } from '@component/client-header/client-header.module'
import { ClientFooterModule } from '@component/client-footer/client-footer.module'
import { IvyCarouselModule } from 'angular-responsive-carousel'
import { ProductDetailService } from './product-detail-page.service'
import { CommonDirectiveModule } from '@directive/common-directive.module'

@NgModule({
  declarations: [ProductDetailPageComponent],
  imports: [
    CommonModule,
    ClientHeaderModule,
    IvyCarouselModule,
    ClientFooterModule,
    CommonDirectiveModule,
    RouterModule,
  ],
  providers: [ProductDetailService],
})
export class ProductDetailPageModule {}
