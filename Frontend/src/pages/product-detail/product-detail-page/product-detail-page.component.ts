import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'
import { FO_Product } from 'shared'

import { ProductDetailService } from './product-detail-page.service'

@Component({
  selector: 'app-product-detail-page',
  templateUrl: './product-detail-page.component.html',
  styleUrls: ['./product-detail-page.component.scss'],
})
export class ProductDetailPageComponent implements OnInit {
  product$: Observable<FO_Product | null>
  constructor(
    private activeRoute: ActivatedRoute,
    private service: ProductDetailService
  ) {
    this.product$ = this.service.getProductById(this.activeRoute.snapshot.params.id)
  }

  ngOnInit(): void {}
}
