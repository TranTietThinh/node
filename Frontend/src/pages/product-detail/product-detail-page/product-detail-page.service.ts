import { Injectable } from '@angular/core'
import { Apollo } from 'apollo-angular'
import { map } from 'rxjs/operators'
import { FO_Product } from 'shared'
import { Observable } from 'rxjs'
import { GetProductByIdQuery, GetProductByIdDocument } from '@graphql'
@Injectable()
export class ProductDetailService {
  constructor(private apollo: Apollo) {}

  getProductById(id: string): Observable<FO_Product | null> {
    return this.apollo
      .query<GetProductByIdQuery>({
        query: GetProductByIdDocument,
        variables: {
          input: id,
        },
      })
      .pipe(map((e) => e.data?.getProductById || null))
  }
}
