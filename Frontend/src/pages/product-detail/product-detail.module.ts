import { NgModule } from '@angular/core'

import { ProductDetailRoutingModule } from './product-detail-routing.module'
import { ProductDetailPageModule } from './product-detail-page/product-detail-page.module'


@NgModule({
  imports: [
    ProductDetailRoutingModule,
    ProductDetailPageModule
  ]
})
export class ProductDetailModule { }
