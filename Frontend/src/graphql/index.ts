import { gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};



export type ProductDetail = {
  __typename?: 'ProductDetail';
  _id: Scalars['String'];
  price: Scalars['Int'];
  name: Scalars['String'];
  scale: Scalars['String'];
  sku: Scalars['String'];
  description: Scalars['String'];
  story: Scalars['String'];
  promotion: Scalars['Int'];
  imgs: Array<Scalars['String']>;
  brandId: Scalars['String'];
};

export type ProductTile = {
  __typename?: 'ProductTile';
  _id: Scalars['String'];
  price: Scalars['Int'];
  name: Scalars['String'];
  scale: Scalars['String'];
  promotion: Scalars['Int'];
  imgs: Array<Scalars['String']>;
  brandId: Scalars['String'];
};

export type BrandModel = {
  __typename?: 'BrandModel';
  _id: Scalars['String'];
  name: Scalars['String'];
  description: Scalars['String'];
  story: Scalars['String'];
  logoImg: Scalars['String'];
};

export type Query = {
  __typename?: 'Query';
  getProductById: ProductDetail;
  getProductsByBrand: Array<ProductTile>;
  getNewArrivalProducts: Array<ProductTile>;
  getTopSellingProducts: Array<ProductTile>;
  getBrandById: BrandModel;
};


export type QueryGetProductByIdArgs = {
  id: Scalars['String'];
};


export type QueryGetProductsByBrandArgs = {
  id: Scalars['String'];
};


export type QueryGetBrandByIdArgs = {
  id: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  createProduct: ProductDetail;
  createBrand: BrandModel;
};


export type MutationCreateProductArgs = {
  input: ProductInput;
};


export type MutationCreateBrandArgs = {
  input: BrandInput;
};

export type ProductInput = {
  price: Scalars['Int'];
  name: Scalars['String'];
  scale: Scalars['String'];
  sku: Scalars['String'];
  description: Scalars['String'];
  story: Scalars['String'];
  promotion: Scalars['Int'];
  imgs: Array<Scalars['String']>;
  brandId: Scalars['String'];
};

export type BrandInput = {
  name: Scalars['String'];
  description: Scalars['String'];
  story: Scalars['String'];
  logoImg: Scalars['String'];
};

export type GetNewArrivalProductsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetNewArrivalProductsQuery = (
  { __typename?: 'Query' }
  & { getNewArrivalProducts: Array<(
    { __typename?: 'ProductTile' }
    & Pick<ProductTile, '_id' | 'price' | 'name' | 'scale' | 'promotion' | 'imgs' | 'brandId'>
  )> }
);

export type GetProductByIdQueryVariables = Exact<{
  input: Scalars['String'];
}>;


export type GetProductByIdQuery = (
  { __typename?: 'Query' }
  & { getProductById: (
    { __typename?: 'ProductDetail' }
    & Pick<ProductDetail, '_id' | 'name' | 'imgs' | 'price' | 'scale' | 'sku' | 'description' | 'story' | 'promotion' | 'brandId'>
  ) }
);

export type GetTopSellingProductsQueryVariables = Exact<{ [key: string]: never; }>;


export type GetTopSellingProductsQuery = (
  { __typename?: 'Query' }
  & { getTopSellingProducts: Array<(
    { __typename?: 'ProductTile' }
    & Pick<ProductTile, '_id' | 'price' | 'name' | 'scale' | 'promotion' | 'imgs' | 'brandId'>
  )> }
);

export const GetNewArrivalProductsDocument = gql`
    query GetNewArrivalProducts {
  getNewArrivalProducts {
    _id
    price
    name
    scale
    promotion
    imgs
    brandId
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetNewArrivalProductsGQL extends Apollo.Query<GetNewArrivalProductsQuery, GetNewArrivalProductsQueryVariables> {
    document = GetNewArrivalProductsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetProductByIdDocument = gql`
    query GetProductById($input: String!) {
  getProductById(id: $input) {
    _id
    name
    imgs
    price
    scale
    sku
    description
    story
    promotion
    brandId
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetProductByIdGQL extends Apollo.Query<GetProductByIdQuery, GetProductByIdQueryVariables> {
    document = GetProductByIdDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const GetTopSellingProductsDocument = gql`
    query GetTopSellingProducts {
  getTopSellingProducts {
    _id
    price
    name
    scale
    promotion
    imgs
    brandId
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class GetTopSellingProductsGQL extends Apollo.Query<GetTopSellingProductsQuery, GetTopSellingProductsQueryVariables> {
    document = GetTopSellingProductsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }