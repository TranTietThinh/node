import { NgModule } from '@angular/core'
import {
  BrowserModule,
  BrowserTransferStateModule,
  makeStateKey,
  TransferState,
} from '@angular/platform-browser'
import { ApolloLink, concat, InMemoryCache } from '@apollo/client/core'
import { Apollo } from 'apollo-angular'
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http'
import { createPersistedQueryLink } from 'apollo-angular-link-persisted'

const STATE_KEY = makeStateKey<any>('apollo.state')

const uri = 'http://localhost:3000/graphql' // <-- add the URL of the GraphQL server here

@NgModule({
  imports: [BrowserModule, HttpLinkModule, BrowserTransferStateModule],
})
export class GraphQLModule {
  cache: InMemoryCache

  constructor(
    apollo: Apollo,
    private readonly transferState: TransferState,
    private httpLink: HttpLink
  ) {
    this.cache = new InMemoryCache()

    const http = httpLink.create({
      uri,
      includeQuery: true,
      includeExtensions: true,
    })

    const authMiddleware = new ApolloLink((operation, forward) => {
      // add the authorization to the headers
      operation.setContext({
        // headers: new HttpHeaders()
        //   .set('Authorization', localStorage.getItem('token') || ''),
        http: {
          includeExtensions: true,
        },
      })
      return forward(operation)
    })
    const nonPersistedApolloLink: ApolloLink = concat(
      concat(createPersistedQueryLink() as any, authMiddleware),
      http as any
    )
    const isBrowser = this.transferState.hasKey<any>(STATE_KEY)
    if (isBrowser) {
      this.onBrowser()
    } else {
      this.onServer()
    }
    // and second one for server
    apollo.create({
      link: nonPersistedApolloLink,
      cache: this.cache,
      ssrForceFetchDelay: 100,
      // ssrMode: false
    })
  }
  onServer(): void {
    this.transferState.onSerialize(STATE_KEY, () => {
      return this.cache.extract()
    })
  }

  onBrowser(): void {
    const state = this.transferState.get<any>(STATE_KEY, null)

    this.cache.restore(state)
  }
}
