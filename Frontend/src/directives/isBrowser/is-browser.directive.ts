import { Directive, TemplateRef, ViewContainerRef, Input } from '@angular/core'

@Directive({
  selector: '[ngIfBrowser]',
})
export class RenderOnClientDirective {
  private hasView = false

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {
    const condition = typeof document !== 'undefined'
    if (condition && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef)
      this.hasView = true
    } else if (condition && this.hasView) {
      this.viewContainer.clear()
      this.hasView = false
    }
  }
}
