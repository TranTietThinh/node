import { NgModule } from '@angular/core'
import { RenderOnClientDirective } from './isBrowser/is-browser.directive'

@NgModule({
  declarations: [RenderOnClientDirective],
  exports: [RenderOnClientDirective],
})
export class CommonDirectiveModule {}
